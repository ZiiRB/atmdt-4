package io.vertx.starter.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.starter.controller.TalabaController;

public class MainRouter {

    private TalabaController talabaController =
            new TalabaController();

    public Router makeRouter(Vertx vertx) {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/talaba").handler(talabaController::getTalabalar);
        router.route("/yigindi/:son1/:son2").handler(talabaController::yigindi);

        return router;
    }
}
